# DevOps

Чангалиди Антон

Запуск докера:
```bash
sudo systemctl start docker
```


## Лаба 1

Задание [тут](lab_description/lab1.pdf).


Проект содержит в себе Airflow пайплайн (`delete_second`), сотоящий из 2 этапов:
- читает `data/input.csv` из директории `data` (можно менять этот файл, так как директория монтируется внутри образа)
- сначала (первый шаг): удаляет каждую вторую колонку, аутпут: `input_temp.csv`
- сначала (второй шаг): удаляет каждую вторую строку, аутпут: `input_out.csv`

### Запуск

```bash
sudo docker compose build
```
```bash
sudo docker compose up
```
Логин и пароль -- дефолтные: `airflow`

## Лаба 2

Задание [тут](lab_description/lab2.pdf).

Проект содержит в себе Airflow пайплайн (`spark_fn`), вызывающий функцию Spark, который, как и в первой лабе удаляет каждую вторую строку и столбец.
__Интересно__, что у меня вышло читать и видоизменять датафрейм, но вот сохранять не вышло - спарк валится на ошибке. В интернете много решений. Самое простое, пока файлы не гигантские - просто перевести в пандас, что я и делаю. 


Для подключения к спарку, воспользуйтесь следующими параметрами конекшна в меню airflow:
* `conn_id=spark_local`
* `conn_type=Spark`
* `host=spark://spark-master`
* `port=7077`

При успешном выполнении дага `spark_fn`, должен появиться файл `out_spark.csv` в директрии `data`.


## Лаба 3

Задание [тут](lab_description/lab3.pdf).


Скопировали в Gitlab'e токен. Вставили его в .env_secret файл под $GITLAB_TOKEN
прочитали, чтобы дальше использовать:

```bash
source .env_secret
```

Для запуска гитлаб'а:
```bash
cd gitlab_runner
sudo docker compose up -d
#docker compose -f docker-compose.yml up -d 
```

Проверка, что все ок:

```bash
docker ps
```

Инициализировали раннер:


```bash
sudo docker exec -it gitlab-runner gitlab-runner register \
--url "https://gitlab.com/" \
--clone-url "https://gitlab.com/" \
--description "TohaRhymesOps" \
--registration-token ${GITLAB_TOKEN} \
--tag-list "docker_812495" \
--executor docker \
--docker-image "docker:stable"
```

```bash
sudo docker exec -it gitlab-runner bash
echo 'volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]' >> /etc/gitlab-runner/config.toml
```


Далее после перезагрузки все готово:
```bash
sudo docker restart gitlab-runner
```

Есть файлик `.gitlab-ci.yml` - благодаря нему работает CI/CD.

Все задания проделаны в этом же файлике. По поводу запуска тжгированных - установлены настройки в гитхабе:

![""](./photo_2023-10-31_19-47-54.jpg)

