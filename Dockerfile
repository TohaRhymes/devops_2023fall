FROM apache/airflow:2.7.1

WORKDIR /opt/airflow

USER root
RUN apt update && apt -y install procps default-jre

USER airflow
COPY requirements.txt ./
COPY dags ./dags
COPY data ./data
RUN pip install -r requirements.txt


