# Release notes

## v1.0 

Добавлена главная функциональность: запуск простого DAG'a в airflow (`delete_second`)

## v2.0 

* В airflow добавлен еще один DAG (`saprk_fn`), поддерживающий запуск Spark функций. 
* Добавлен файл для работы в spark: ``spark/spark_app`` с аналогичной первой лабе функциональностью.
* Изменен `Dockerfile` & `docker-compose.yaml`, сейчас они включают также образы для спарк воркера и мастера.

## v3.0 

* Добавлен файл `docker-compose-gitlab.yml` - для запуска гитлаба. 
* Добавлен файл `gitlab_runner/.gitlab-ci.yml` -- для развертки пайплайна.
* Обновлен readme