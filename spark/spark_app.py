from pyspark.sql import SparkSession, Row
from pyspark import SparkContext, SparkConf

import csv


def main():
    conf = SparkConf().setAppName("PySpark attempt").setMaster("spark://spark-master:7077")
    spark = SparkSession.builder \
        .appName("Delete Every Second Row and Column").config(conf=conf) \
        .getOrCreate()

    input_path = "/opt/airflow/data/input.csv"
    output_path = "/opt/airflow/data/out_spark.csv"

    # Read CSV data
    data = spark.read.csv(input_path, header=False, inferSchema=True)

    # Delete every second row
    rdd = data.rdd.zipWithIndex().filter(lambda row: row[1] % 2 == 0).map(lambda row: row[0])
    data = spark.createDataFrame(rdd, data.schema)

    # Delete every second column
    columns_to_select = data.columns[::2]
    data = data.select(*columns_to_select)

    data.toPandas().to_csv(output_path, header=False, index=False)

    spark.stop()


main()
