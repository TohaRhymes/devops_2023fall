from datetime import datetime, timedelta
from airflow import DAG
from airflow.contrib.operators.spark_submit_operator import SparkSubmitOperator

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
}

dag = DAG(
    'spark_fn',
    default_args=default_args,
    description='Simple spark dag',
    schedule_interval='@daily',
    start_date=datetime(2023, 10, 19),
    catchup=False,
)

# SparkSubmitOperator task
spark_task = SparkSubmitOperator(
    task_id='spark_fn_job',
    name='spark_fn_name',
    application='/opt/airflow/spark/spark_app.py',
    conn_id='spark_local',
    verbose=True,
    dag=dag
)

# Set this task as the starting point
spark_task
