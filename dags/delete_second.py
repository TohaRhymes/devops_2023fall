import pandas as pd
import os
from datetime import datetime
from airflow import DAG
from airflow.operators.python_operator import PythonOperator


def append_postfix(filename, postfix):
    name, ext = os.path.splitext(filename)
    return f"{name}_{postfix}{ext}"


def delete_columns(infile, outfile):
    df = pd.read_csv(infile, header=None)
    cols_to_drop = df.columns[1::2]
    df.drop(columns=cols_to_drop, inplace=True)
    df.to_csv(outfile, index=False, header=False)


def delete_rows(infile, outfile):
    df = pd.read_csv(infile, header=None)
    df = df.iloc[::2]
    df.to_csv(outfile, index=False, header=False)


def get_names(filename):
    out1 = append_postfix(filename, "temp")
    out2 = append_postfix(filename, "out")
    return filename, out1, out2


default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
}

dag = DAG(
    'process_file_dag',
    default_args=default_args,
    description='Process file in two steps: delete columns, then delete rows',
    schedule_interval='@daily',
    start_date=datetime(2023, 10, 19),
    catchup=False,
)

FILENAME = "/opt/airflow/data/input.csv"
infile, tempfile, outfile = get_names(FILENAME)

t1 = PythonOperator(
    task_id='delete_columns_task',
    python_callable=delete_columns,
    op_args=[infile, tempfile],
    dag=dag,
)

t2 = PythonOperator(
    task_id='delete_rows_task',
    python_callable=delete_rows,
    op_args=[tempfile, outfile],
    dag=dag,
)

t1 >> t2

if __name__ == "__main__":
    FILENAME = "/opt/airflow/data/input.csv"
    infile, outfile1, outfile2 = get_names(FILENAME)
    delete_columns(infile, outfile1)
    delete_rows(outfile1, outfile2)
